from tkinter import *
from tkinter import ttk

root = Tk()

c = ttk.Frame(root, padding=(5, 5, 12, 0))
c.grid(column=0, row=0, sticky=(N,W,E,S))
root.grid_columnconfigure(0, weight=1)
root.grid_columnconfigure(1, weight=1)
root.grid_rowconfigure(0,weight=1)

tree = ttk.Treeview(c)
tree.grid(column=0, row=0, sticky=(N,W,E,S))

#tree["columns"] = ("one", "two")
#tree.column("one", width=100)
#tree.column("two", width=100)
#tree.heading("one", text="column A")
#tree.heading("two", text="column B")

# Args are (parent, index, id) and possibly text and values
# If you don't specify an ID, one will be returned
# Empty string means the root
tree.insert("", 0, text="Line 1")

id2 = tree.insert("", 1, "dir2", text="Dir 2")
tree.insert(id2, "end", "dir 2", text="sub dir 2")

##alternatively:
tree.insert("", 3, "dir3", text="Dir 3")
tree.insert("dir3", 3, text=" sub dir 3")

txt = Text(c, width=40, height=5)
txt.grid(column=1, row=0)

root.mainloop()