from tkinter import *
from tkinter import ttk
import os, sys

def newFile():
    print('Selected new file')

def exitProgram():
    sys.exit(0)

root = Tk()
root.title('Menus')

win = Toplevel(root)           # Get the toplevel window
menubar = Menu(win)            # Create a Menu widget
win['menu'] = menubar          # Attach the menu to the window
win.title('New')               # Set the title

menu_file = Menu(menubar)
menu_edit = Menu(menubar)
menubar.add_cascade(menu=menu_file, label='File')
menubar.add_cascade(menu=menu_edit, label='Edit')

menu_file.add_command(label='New', command=newFile, accelerator="Command-Q")
menu_file.add_separator()
menu_file.add_command(label='Quit', command=exitProgram, accelerator="Command-Q")

root.mainloop()
