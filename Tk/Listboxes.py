from tkinter import *
from tkinter import ttk
import os, sys

countrynames = ('Argentina', 'Australia', 'Belgium', 'Brazil', 'Canada', 'China', 'Denmark', \
        'Finland', 'France', 'Greece', 'India', 'Italy', 'Japan', 'Mexico', 'Netherlands', 'Norway', 'Spain', \
        'Sweden', 'Switzerland')

root = Tk()
root.title('Listbox')

cnames = StringVar(value=countrynames)

c = ttk.Frame(root, padding=(5, 5, 12, 0))
c.grid(column=0, row=0, sticky=(N,W,E,S))
root.grid_columnconfigure(0, weight=1)
root.grid_rowconfigure(0,weight=1)

lbox = Listbox(c, listvariable=cnames, height=5)
lbox.grid(column=0, row=0, rowspan=6, sticky=(N,S,E,W))

root.mainloop()
