from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from tkinter import colorchooser
from tkinter import messagebox

def getFilename():
    filename = filedialog.askopenfilename()

def getColor():
    colorchooser.askcolor(initialcolor='#ff0000')

def showAlert():
    messagebox.askyesno(message='Are you sure you want to install SuperVirus?',
        title = 'Install')

root = Tk()
root.title('Dialogs')
#root.geometry("300x200")

c = ttk.Frame(root, padding=(5, 5, 5, 5))
c.grid(column=0, row=0, sticky=(N,W,E,S))
root.grid_columnconfigure(0, weight=1)
root.grid_columnconfigure(1, weight=1)
root.grid_rowconfigure(0,weight=1)
root.grid_columnconfigure(1, weight=1)

btnFile = ttk.Button(c, text='Open File', command=getFilename)
btnFile.grid(column=0, row=0)

btnColor = ttk.Button(c, text='Choose Color', command=getColor)
btnColor.grid(column=1, row=0)

btnAlert = ttk.Button(c, text='Show Alert', command=showAlert)
btnAlert.grid(column=2, row=0)

root.mainloop()
