from tkinter import *
from tkinter import ttk
from tkinter import messagebox

import os, sys

useMetric = True

def acceptTCs():
    OKbtn.configure(state='normal')

def dialog():
    var = messagebox.showinfo("Great!" , "Now we can continue")

root = Tk()
root.title('Checkboxes')
#root.geometry("300x300")

c = ttk.Frame(root, padding=(5, 5, 12, 0))
c.grid(column=0, row=0, sticky=(N,W,E,S))
root.grid_columnconfigure(0, weight=1)
root.grid_rowconfigure(0,weight=1)
root.grid_rowconfigure(1,weight=1)
root.grid_rowconfigure(2,weight=1)

lbl = ttk.Label(c, text='Please accept the terms and conditions before continuing...')
lbl.grid(column=0, row=0)

TCs = StringVar()
check = ttk.Checkbutton(c, text='I accept the T&Cs',
                        command=acceptTCs, variable=TCs,
                        onvalue='accept', offvalue='reject')

check.grid(column=0, row=1)

OKbtn = ttk.Button(text='OK', command=dialog)
OKbtn.configure(state='disabled')
OKbtn.grid(column=0, row=2)

root.mainloop()
