from tkinter import *
from tkinter import ttk
import random
import os, sys

country = ""
flag = ""
img = None

def check(*args):
    global country, flag
    c = guess.get()
    if c.upper() == country.upper():
        answerlbl['text'] = 'Correct!'
    else:
        answerlbl['text'] = 'Sorry - it was ' + country

    # load and display new flag, blank input and answer
    country, flag = load_country()
    img = PhotoImage(file=flag)
    lbl.config(image = img)
    lbl.image = img
    name.delete(0, END)

def load_country():
    rnd = random.randint(0, len(countries) - 1)
    country, flag = countries[rnd]
    print('%s: %s' % (country, flag))
    return (country, flag)


# Not using a map, because indexing into maps is a pain
countries = [('UK', 'UK-flag.gif'), ('US', 'US-flag.gif'), ('Albania', 'Albania-flag.gif'), ('Armenia', 'Armenia-flag.gif'),
             ('Bulgaria', 'Bulgaria-flag.gif'), ('Chad', 'Chad-flag.gif'), ('Cuba', 'Cuba-flag.gif'), ('Guyana', 'Guyana-flag.gif'),
             ('Jordan', 'Jordan-flag.gif'), ('Kazakhstan', 'Kazakhstan-flag.gif'), ('Kosovo', 'Kosovo-flag.gif'), ('Laos', 'Laos-flag.gif'),
             ('Latvia', 'Latvia-flag.gif'), ('Macedonia', 'Macedonia-flag.gif'), ('Maldives', 'Maldives-flag.gif'), ('Micronesia', 'Micronesia-flag.gif'),
             ('Nepal', 'Nepal-flag.gif'), ('Qatar', 'Qatar-flag.gif'), ('St.Vincent', 'St.Vincent-flag.gif'), ('Tuvalu', 'Tuvalu-flag.gif'),
             ('Ukraine', 'Ukraine-flag.gif'), ('Uzbekistan', 'Uzbekistan-flag.gif'), ('Vanuatu', 'Vanuatu-flag.gif'), ('Vatican', 'Vatican-flag.gif')]

root = Tk()
root.title("Tk Test")

guess = StringVar()

# pick a flag
country, flag = load_country()

mainframe = ttk.Frame(root, padding="3 3 12 12", width="500", height="500")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

namelbl = ttk.Label(mainframe, text="Country?")
namelbl.grid(column=3, row=0, columnspan=2, sticky=(N, W), padx=5)
name = ttk.Entry(mainframe, textvariable=guess)
name.grid(column=3, row=1, columnspan=2, sticky=(N, E, W), pady=5, padx=5)
answerlbl = ttk.Label(mainframe, text=" ")
answerlbl.grid(column=3, row=2, columnspan=2, sticky=(N, W), padx=5)

img = PhotoImage(file=flag)
lbl = ttk.Label(mainframe, image=img, padding="10 10 10 10")
lbl.grid(column=1, row=1, rowspan=2)
# Hack because tkinter doesn't handle images properly
# see http://effbot.org/pyfaq/why-do-my-tkinter-images-not-appear.htm
lbl.image = img

ok = ttk.Button(mainframe, text="Try", command=check)
ok.grid(column=3, row=3)

root.mainloop()