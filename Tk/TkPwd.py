from tkinter import *
from tkinter import ttk

def check_login():
    xx = pwd.get()

root = Tk()
root.title("Login")

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

pwd = StringVar()

pwd_entry = ttk.Entry(mainframe, width=15, textvariable=pwd, show='*')
pwd_entry.grid(column=1, row=0, sticky=(W, E))
ttk.Label(mainframe, text="Password: ").grid(column=0, row=0, sticky=W)

ttk.Button(mainframe, text="Login", command=check_login).grid(column=2, row=0, sticky=W)

root.mainloop()
