from tkinter import *
from tkinter import ttk

root = Tk()
root.title('Text Entry')
#root.geometry("300x200")

c = ttk.Frame(root, padding=(5, 5, 5, 5))
c.grid(column=0, row=0, sticky=(N,W,E,S))
root.grid_columnconfigure(0, weight=1)
root.grid_rowconfigure(0,weight=1)

txt = Text(c, width=40, height=5)
txt.grid(column=0, row=0)

txt.insert(INSERT, 'Hello!')
txt.insert(END, '.....')

root.mainloop()
