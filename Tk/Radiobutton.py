from tkinter import *
from tkinter import ttk
from tkinter import messagebox

import os, sys

def dialog():
    if animal.get() == 'ferret':
        var = messagebox.showinfo("Great!" , "That's the right answer")
    else:
        var = messagebox.showinfo("Sorry..." , "That isn't correct")

root = Tk()
root.title('Radiobuttons')
#root.geometry("300x300")

c = ttk.Frame(root, padding=(5, 5, 12, 0))
c.grid(column=0, row=0, sticky=(N,W,E,S))
root.grid_columnconfigure(0, weight=1)
root.grid_rowconfigure(0,weight=1)
root.grid_rowconfigure(1,weight=1)
root.grid_rowconfigure(2,weight=1)

lbl = ttk.Label(c, text='"Mustela putorius furo" is the Latin name for which mammal?')
lbl.grid(column=0, row=0)

animal = StringVar()
rb1 = ttk.Radiobutton(c, text='Stoat', variable=animal, value='stoat')
rb2 = ttk.Radiobutton(c, text='Weasel', variable=animal, value='weasel')
rb3 = ttk.Radiobutton(c, text='Ferret', variable=animal, value='ferret')
rb4 = ttk.Radiobutton(c, text='Pine Marten', variable=animal, value='marten')

rb1.grid(column=0, row=1)
rb2.grid(column=0, row=2)
rb3.grid(column=0, row=3)
rb4.grid(column=0, row=4)

OKbtn = ttk.Button(text='Try', command=dialog)
OKbtn.grid(column=0, row=5)

root.mainloop()
