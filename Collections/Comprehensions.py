# Example showing the use of for comprehensions on lists
prices = [12, 22, 14, 5, 7, 15]

new_prices = []

# This is using a traditional loop
# Each element in prices is copied into price as the loop progresses
for price in prices:
    new_prices.append(price + price*0.2)

print(new_prices)

# A comprehension automatically creates a new list by transforming
# each element of the old one. You can also use 'if' to provide filtering
prices2 = [price + price*0.2 for price in prices if price > 14]

print(prices2)

# Collection Functions
def startsWithB(element):
  if len(element) and element[0] == 'B':
    return True
  else:
    return False

def topAndTail(element):
    return "***" + element + "***"

names = ["Zak", "Tim", "Ben", "Joe", "Kim", "Bud", "Ted", "Baz"]

bnames = list(filter(startsWithB, names))
print(bnames)

sortedBnames = sorted(bnames)
print(sortedBnames)

mappedSortedBnames = list(map(topAndTail, sortedBnames))
print(mappedSortedBnames)

nameLengths = list(map(len, names))
print(nameLengths)
