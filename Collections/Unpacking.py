# Multiple assignment
# 2 is assigned to 'x' and 3 to 'y'
x, y = (2, 3)

print(x)
print(y)

# Used to return more than one thing from a function, by
# wrapping it in a tuple
def divider(a, b):
    n = a // b
    m = a % b
    return (n, m)

val = divider(10, 3)
print(val)

# Unpack the items from the tuple
q, r = divider(10, 3)

# Tell Python we're not interested in the first item
_, e = (2, 3)
print(e)



