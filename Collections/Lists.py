lst = [1, 3, 5, 7, 9, 11, 13, 15, 17]

print(lst)

# Access elements using square brackets, zero-indexed
lst[3] = 8

print(lst)

# Lists can contain other lists
lst2 = [[1, 2, 3], ['ab', 'cd']]

# Accessing elements in a nested list
print(lst2[0][1])
print('length of list[1]: %d' % len(lst2[1]))

# Just giving one index gives you the whole sublist
print(lst2[0])

# Tuples use round brackets
tup = (1, 3, 5, 7, 9, 11)

print(tup)

# This isn't a one-element tuple...
x = (3)
# But this is
y = (3,)

print(x)

# Take a slice in reverse order
lst3 = lst[-1::-2]

print(lst3)

# It works with strings, too
s = "Hello world! This is Python"
ss = s[-1::-2]

print(ss)