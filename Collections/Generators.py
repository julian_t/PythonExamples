# Return a collection of numbers.

def getNumsA(n):
    num, nums = 0, []
    while num < n:
        nums.append(num)
        num += 1
    return nums

n = getNumsA(10)
print(n)
print(sum(n))

# Equivalent effect using a generator.

def getNumsB(n):
    num = 0
    while num < n:
        yield num
        num += 1

n1 = getNumsB(10)
print(n1)
print(sum(n1))