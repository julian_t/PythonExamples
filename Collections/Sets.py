# Sets don't contain duplicate items, so this will have three members
set1 = {'dog', 'cat', 'ant', 'cat', 'dog'}

print(set1)

# A tuple could contain duplicates, and have items of different types
t1 = (123, 1000.00, "USD", "ibm.n")

# A dictionary consists of key/value pairs

d1 = {"UK": "London", "France": "Paris", "Hungary": "Budapest"}

print(d1)

countries = ['Japan', 'China', 'USA']
capitals = ['Tokyo', 'Beijing', 'Washington DC']

# 'zip' combines two lists into a map. The first contains the keys, the 
# second contains the values
d2 = dict(zip(countries, capitals))
print(d2)

# Access elements using square brackets with the key
# If no such key, you'll get an error
print(d1["Hungary"])

# Values can be more complex types
d3 = {"US": ("USD", "Washington"), "UK": ("GBP", "London")}
print(d3["UK"])

d4 = {
    "Customers": [
        {"Name": "fred", "City": "London"},
        {"Name": "dave", "City": "Bermuda"}]}

print(d4["Customers"][0]["Name"])
