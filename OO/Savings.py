from Account import MyAccount

# SavingsAccount that inherits from MyAccount
# It calls the superclass __init__, but otherwise just adds an
# interest rate member
class SavingsAccount(MyAccount):
    def __init__(self, acc_no, opening_balance, rate):
        super().__init__(acc_no, opening_balance)
        self.__rate = rate