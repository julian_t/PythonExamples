from datetime import datetime

# A simple Employee class
class Employee:
    """This is the Employee class"""
    # The class defines a minimum salary of 12000
    __minimum_salary = 12000

    # Initialize the object, and set the salary to at least the minimum
    def __init__(self, name, salary):
        self.__name = name

        self.__salary = salary if salary >= Employee.__minimum_salary else Employee.__minimum_salary
        self.__joining_date = datetime.now()

    # Call this when an object is being destroyed
    def __del__(self):
        print('Employee being destroyed')

    # Human-readable representation
    def __str__(self):
        return 'Name: {0}, Salary {1}, Joined: {2}'.format(self.__name, self.__salary, self.__joining_date)

    def get_name(self):
        return self.__name

    def get_salary(self):
        return self.__salary

    def get_joining_date(self):
        return self.__joining_date

    def pay_raise(self, amount):
        self.__salary += amount

    # Only pay a bonus if the salary is within the range
    def pay_bonus(self, percentage=1, min=1000, max=50000):
        if self.__salary >= min and self.__salary <= max:
            bonus = self.__salary * percentage / 100
            self.__salary += bonus
        else:
            print('No bonus available')
