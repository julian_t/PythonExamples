from Account import MyAccount
from Savings import SavingsAccount

# Create an account object
acc1 = MyAccount(12345, 0.0)

acc1.print_balance()

# Create another one
acc2 = MyAccount(22234, 10.0)
acc2.deposit(20.0, 'Cash')  # MyAccount.deposit(acc2, 20.0)
acc2.withdraw(5.0, 'Lunch')

acc2.print_balance()

acc2.print_transactions()

# all my accounts
accounts = [acc1, acc2]

total_balance = 0.0
for a in accounts:
    total_balance = total_balance + a.get_balance()
print(total_balance)

print(MyAccount.get_interest_rate())

# Create a SavingsAccount
sav1 = SavingsAccount(5555, 0.0, 0.5)
print(sav1.get_balance())