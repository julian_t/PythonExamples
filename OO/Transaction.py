# A simple class to represent a transaction on an account

from datetime import datetime

class Transaction:
    """Class to represent a transaction on an account"""
    def __init__(self, amount, deposit, description):
        """Transaction takes three parameters:
           amount: the absolute amount
           deposit: True if this was a deposit
           description: a description for this transaction
        """
        self.__amount = amount
        self.__deposit = deposit
        self.__description = description
        self.__time = datetime.now()
    
    # Define getters for the fields
    def amount(self):
        return self.__amount

    def is_deposit(self):
        return self.__deposit

    def description(self):
        return self.__description

    def time(self):
        return self.__time

    # Represent the transaction as a string
    def __str__(self):
        sign = '' if self.is_deposit() else '-'
        return '{0}{1} on {2} ({3})'.format(sign, self.__amount, self.__time, self.__description)
