from Employees import Employee

emp1 = Employee("fred", 10000)
print('%s earns %d, joined on %s' % (emp1.get_name(), emp1.get_salary(), emp1.get_joining_date()))

emp1.pay_bonus(percentage=2)
print('%s now earns %d' % (emp1.get_name(), emp1.get_salary()))

print(emp1.__dict__)

print(emp1)