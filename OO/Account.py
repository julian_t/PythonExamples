from Transaction import Transaction

# A simple bank account class
class MyAccount:
    """Bank Account class"""

    # Belongs to MyAccount, accessible to all objects
    __interest_rate = 0.5

    # Function for initializing account objects when they are created
    def __init__(self, acc_number, opening_balance=0.0):
        """
            Two parameters:
            acc_number - the account number
            opening_balance - the opening balance for the account
        """
        print('creating an account: %d with balance %f' % (acc_number, opening_balance))
        # The account_number is public
        self.account_number = acc_number
        # The balance is private, because of the "__"
        self.__balance = opening_balance
        # Create an empty list of transactions
        self.__transactions = []           
        # Opening an account gives you the first transaction
        self.__transactions.append(Transaction(opening_balance, True, 'Account opened'))

    def deposit(self, amount, desc):
        if amount > 0.0:
            self.__balance += amount
            # Record the transaction
            self.__transactions.append(Transaction(amount, True, desc))

    def withdraw(self, amount, desc):
        if amount > 0.0 and self.__balance - amount >= 0.0:
            self.__balance -= amount
            # Record the transaction
            self.__transactions.append(Transaction(amount, False, desc))

    def get_balance(self):
        return self.__balance

    def print_balance(self):
        print('Balance for %d is %f' % (self.account_number, self.__balance))

    def print_transactions(self):
        """Print the transactions in the list"""
        for t in self.__transactions:
            print(t)

    def get_interest_rate():
        """Class-level function to return interest rate"""
        return MyAccount.__interest_rate