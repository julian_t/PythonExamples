# A simple example to show the use of regular expressions

# Import the regular expression module
import re

# Compile an expression to match this string
reObj = re.compile('Attribute ID \(0xC2\)')

# A function to search each line for the pattern. If we get a result,
# the line is printed
def process_line(line):
    result = reObj.search(line)
    if result:
        print(line)

# Open the file. This returns a 'file handle' that we use to read it.
# The file is treated as a collection of lines, so we can use a
# 'for' loop
with open('data.txt') as fh:
    for line in fh:
        process_line(line)

