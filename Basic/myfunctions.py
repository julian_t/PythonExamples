# A module containing functions, to be used as a library by importing it
# See 'functions.py' for code that uses this

def say_hello(name):
    """
        (This is a documentation comment for a function)
        Prints a greeting.
        Arguments: name (string) - the name to print
    """
    print('hello ' + name + '!')

def add(a, b):
    return a+b

def generate_hyperlink(href, text):
    """ Generate a formatted link.
        Could also be done in one line
        return "<a href='{0}'>{1}</a>".format(href, text)
    """
    link = "<a href='{0}'>{1}</a>"
    formatted_link = link.format(href, text)
    return formatted_link

# You might add a main to a library to hold test code
def main():
    print('this is myfunctions')

if __name__ == '__main__':
    main()