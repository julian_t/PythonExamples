# Import functions from module 'myfunctions'
from myfunctions import say_hello, add, generate_hyperlink

def main():
    print('Functions')

    x = say_hello('fred')

    val = add(2, 3)
    print(val)

    print(add('ab', 'cd'))

    hlink = generate_hyperlink('http://www.bbc.co.uk', 'BBC')
    print(hlink)

# Are we being run as a program or a library?
# This is a common idiom - put the main script in a function and
# run it like this
if __name__ == '__main__':
    main()