# If we want to add varying numbers of arguments, we could define separate
# functions to do it
def add2(a, b):
    return a+b

def add3(a, b, c):
    return a+b+c

def add4(a, b, c, d):
    return a+b+c+d

# Or we could use a 'variadic function'
# These can take any number of arguments. An argument with an asterisk in
# front of the name is passed in as a collection
def add(*nums):
    total = 0
    # Use nums as a collection with a 'for' loop
    for n in nums:
        total = total + n
    return total

val = add(1, 2, 3, 4, 5, 6, 7, 8)
print(val)