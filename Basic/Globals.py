# Example to show use of global and local variables
# __MYVAR is a global (remember that constants are given all upper-case names)
__MYVAR = 'global'

def func():
    print('in func()')
    # This defines a local variable
    # It hides (shadows) the global with the same name
    __MYVAR = 'local'
    print(__MYVAR)

def func2():
    # func2 doesn't have __MYVAR, so it uses the global one
    print(__MYVAR)

def func3():
    print('in func3()')
    # Declare __MYVAR as a global
    global __MYVAR
    # This uses the global variable
    __MYVAR = 'func3!'
    print(__MYVAR)

print('starting script')
func()

print(__MYVAR)

print('calling func2')
func2()

print('calling func3')
func3()
