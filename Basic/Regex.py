# A more complex example showing how to use regular expressions to parse
# a data file

import re

# ^         matches the start of the line
# ()        captures a group of characters for later use
#           These will be called 'group(n)' later on, starting from 1
# [^ ]      matches anything that isn't a space
# [^ ]*     matches zero or more non-spaces
# [ \t]*    matches zero or more space or tab
pattern = re.compile('^([^ ]*)[ \t]*([^ ]*)[ \t]*([^ ]*)[ \t]*([^ ]*)')

# 'Status Flags:' matches anywhere in the line
# [ \t]* matches zero or more space or tab
# ([^ \t\n]*) matches any number of chars that aren't space or tab, and
#   captures them for future use
status_pattern = re.compile('Status Flags:[ \t]*([^ \t\n]*)')

with open('data.txt') as fh:
    for line in fh:
        # Does the line contain 'Status Flags'?
        status_flags = "Status Flags" in line
        # Search for the first pattern to capture the first four fields
        result = pattern.search(line)
        if result:
            # Print out the four fields separately
            print('1: %s\n2: %s' % (result.group(3), result.group(4)))
            # Put the four fields into a tuple
            fields = (result.group(1), result.group(2), result.group(3), result.group(4))
            print(fields)
        # If we have a 'status flags' line, extract the last field
        if status_flags:
            m = status_pattern.search(line)
            print('flags: ' + m.group(1))

